package com.bae.jshilt.di

import com.bae.jshilt.utils.Calculator
import com.bae.jshilt.viewmodel.CalculatorViewModelFactoryImpl
import com.bae.jshilt.viewmodel.MainViewModelFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent

@InstallIn(ActivityComponent::class)
@Module
class ActivityModule
{
    @Provides
    fun provideCalculatorViewModelFactory(calculator: Calculator): MainViewModelFactory =
        CalculatorViewModelFactoryImpl(calculator)
}