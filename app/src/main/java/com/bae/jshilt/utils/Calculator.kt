package com.bae.jshilt.utils

class Calculator {
    fun minus(a: Int, b: Int) = a - b
    fun sum(a: Int, b: Int) = a + b
    fun mul(a: Int, b: Int) = a * b
}