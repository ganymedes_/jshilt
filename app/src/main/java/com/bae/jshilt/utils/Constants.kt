package com.bae.jshilt.utils

object Constants
{
    const val BASE_URL = "https://www.googleapis.com/"
    const val API_ENDPOINT = "books/v1/volumes"

    const val WORD = "q"
    const val START_INDEX = "startIndex"
    const val MAX_RESULT = "maxResults"

    const val WORD_VALUE = "ドラゴンボール"
    const val START_INDEX_VALUE = 0
    const val MAX_RESULT_VALUE = 20
}