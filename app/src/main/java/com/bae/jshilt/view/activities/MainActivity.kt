package com.bae.jshilt.view.activities

import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bae.jshilt.databinding.ActivityMainBinding
import com.bae.jshilt.viewmodel.MainViewModel
import com.bae.jshilt.viewmodel.MainViewModelFactory
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity()
{
    private lateinit var binding: ActivityMainBinding

    @Inject lateinit var viewModelFactory: MainViewModelFactory
    private lateinit var viewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        viewModel = ViewModelProvider(this, viewModelFactory).get(MainViewModel::class.java)
        viewModel.result.observe(this, Observer {
            binding.tvResult.text = it.toString()
        })

        viewModel.msg.observe(this, Observer {
            it.getContentIfNotHandled()?.let { msg ->
                showMessage(msg)
            }
        })
    }

    fun onMinusClick(view: View) {
        hideKeyboard()
        viewModel.onMinusClick(binding.etA.text.toString().trim(), binding.etB.text.toString().trim())
    }

    private fun hideKeyboard() {
        val view = this.currentFocus
        view?.let { v ->
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
            imm?.hideSoftInputFromWindow(v.windowToken, 0)
        }
    }

    private fun showMessage(@StringRes msgId: Int) {
        Snackbar.make(binding.rootView, msgId, Snackbar.LENGTH_LONG).show()
    }
}