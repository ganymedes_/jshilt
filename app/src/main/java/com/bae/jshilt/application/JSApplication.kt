package com.bae.jshilt.application

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class JSApplication: Application()
{

}