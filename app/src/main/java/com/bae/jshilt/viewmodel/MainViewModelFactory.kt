package com.bae.jshilt.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.bae.jshilt.utils.Calculator

interface MainViewModelFactory: ViewModelProvider.Factory

@Suppress("UNCHECKED_CAST")
class CalculatorViewModelFactoryImpl(private val calculator: Calculator): MainViewModelFactory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MainViewModel(calculator) as T
    }
}