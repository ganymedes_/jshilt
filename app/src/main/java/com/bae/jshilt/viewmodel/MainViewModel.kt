package com.bae.jshilt.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.bae.jshilt.R
import com.bae.jshilt.utils.Calculator
import com.bae.jshilt.utils.Event
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.scopes.ViewModelScoped
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(private val calculator: Calculator): ViewModel()
{
    // result livedata
    private val _result = MutableLiveData<Int>().apply { value = 0 }
    val result: LiveData<Int>
        get() = _result

    // Message livedata
    private val _msg = MutableLiveData<Event<Int>>()
    val msg: LiveData<Event<Int>>
        get() = _msg

    // Sum method
    fun onSumClick(textA: String, textB: String) {
        if (textA.isEmpty()) {
            _result.value = 0
            _msg.value =
                Event(R.string.msg_input_a)
            return
        }

        if (textB.isEmpty()) {
            _result.value = 0
            _msg.value =
                Event(R.string.msg_input_b)
            return
        }

        _result.value = calculator.sum(textA.toInt(), textB.toInt())
    }

    // Minus method
    fun onMinusClick(textA: String, textB: String) {
        if (textA.isEmpty()) {
            _result.value = 0
            _msg.value =
                Event(R.string.msg_input_a)
            return
        }

        if (textB.isEmpty()) {
            _result.value = 0
            _msg.value =
                Event(R.string.msg_input_b)
            return
        }

        _result.value = calculator.minus(textA.toInt(), textB.toInt())
    }
}