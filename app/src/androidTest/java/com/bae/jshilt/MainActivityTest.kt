package com.bae.jshilt

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.test.core.app.launchActivity
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.bae.jshilt.di.ActivityModule
import com.bae.jshilt.utils.Event
import com.bae.jshilt.view.activities.MainActivity
import com.bae.jshilt.viewmodel.MainViewModel
import com.bae.jshilt.viewmodel.MainViewModelFactory
import dagger.hilt.android.testing.BindValue
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.UninstallModules
import io.mockk.every
import io.mockk.mockk
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@Suppress("UNCHECKED_CAST")
@HiltAndroidTest
@UninstallModules(ActivityModule::class)
@RunWith(AndroidJUnit4::class)
class MainActivityTest {
    private val viewModel = mockk<MainViewModel>(relaxed = true)

    @BindValue
    @JvmField
    val viewModelFactory: MainViewModelFactory = object : MainViewModelFactory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return viewModel as T
        }
    }

    private val result = MutableLiveData<Int>()
    private val msg = MutableLiveData<Event<Int>>()

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @Before
    fun setUp() {
        every { viewModel.result } returns result
        every { viewModel.msg } returns msg
    }

    @Test
    fun test_Display_When_Minus_LiveDataChange() {
        //Given
        val scenario = launchActivity<MainActivity>()

        //When
        result.postValue(10)

        //Then
        onView(withId(R.id.tv_Result)).check(matches(withText("10")))
    }

    @Test
    fun test_ShowMessage_When_Message_LiveDataChange() {
        //Given
        val scenario = launchActivity<MainActivity>()

        //When
        msg.postValue(Event((R.string.msg_input_a)))

        //Then
        onView(withId(com.google.android.material.R.id.snackbar_text))
            .check(matches(withText(R.string.msg_input_a)))
        onView(withId(R.id.tv_Result)).check(matches(withText("0")))
    }
}