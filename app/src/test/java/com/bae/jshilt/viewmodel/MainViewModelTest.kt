package com.bae.jshilt.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.bae.jshilt.R
import com.bae.jshilt.getOrAwaitValue
import com.bae.jshilt.utils.Calculator
import io.mockk.every
import io.mockk.mockk
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito.verify

class MainViewModelTest
{
    private lateinit var calculator: Calculator
    private lateinit var viewModel: MainViewModel

    private val textA = "9"
    private val a = textA.toInt()
    private val textB = "6"
    private val b = textB.toInt()
    private val result = a - b

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        calculator = mockk<Calculator>()
        viewModel = MainViewModel(calculator)
    }

    @Test
    fun test_MinusReturnFromCalculator_LiveDataChanged() {
        //Given
        every { calculator.minus(a, b) } returns result

        //When
        viewModel.onMinusClick(textA, textB)

        //Then
        verify { calculator.minus(a, b) }
        assertEquals(result, viewModel.result.getOrAwaitValue())
    }

    @Test
    fun empty_TextA_Show_Message() {
        // Given
        every { calculator.minus(a, b) } returns result

        // When
        viewModel.onMinusClick("", textB)

        // Then
        assertEquals(0, viewModel.result.getOrAwaitValue())
        assertEquals(R.string.msg_input_a, viewModel.msg.getOrAwaitValue().peekContent())
    }

    @Test
    fun empty_TextB_Show_Message() {
        // Given
        every { calculator.minus(a, b) } returns result

        // When
        viewModel.onMinusClick(textA, "")

        // Then
        assertEquals(0, viewModel.result.getOrAwaitValue())
        assertEquals(R.string.msg_input_b, viewModel.msg.getOrAwaitValue().peekContent())
    }
}